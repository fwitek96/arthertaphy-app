$env:COMPOSE_PROJECT_NAME = New-Guid
docker-compose up -d postgres
Start-Sleep -Seconds 5
docker-compose up -d keycloak
Read-Host -Prompt 'Press Enter to exit...'
docker-compose down --remove-orphans --rmi local