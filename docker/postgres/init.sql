CREATE USER keycloak WITH PASSWORD "keycloak";
CREATE DATABASE keycloak;
GRANT ALL PRIVILEGES ON DATABASE keycloak TO keycloak;
\connect keycloak
CREATE EXTENSION "uuid-ossp";
ALTER SCHEMA public OWNER TO keycloak;

CREATE USER backend WITH PASSWORD 'backend';
CREATE DATABASE backend;
GRANT ALL PRIVILEGES ON DATABASE backend TO backend;
\connect backend
CREATE EXTENSION "uuid-ossp";
ALTER SCHEMA public OWNER TO backend;